import { CompilingOptions } from "../models/compiling-options";
export declare function transpile(filePath: any, options: CompilingOptions): Promise<string[]>;
