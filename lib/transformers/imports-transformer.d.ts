import { CompilingOptions } from "../models/compiling-options";
export declare function importsTransformerFactory(filePath: string, options: CompilingOptions): (context: any) => (node: any) => any;
