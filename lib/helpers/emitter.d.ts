export declare class Emitter {
    private message;
    constructor(message: string);
    done(): void;
}
